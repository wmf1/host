import React, { Suspense } from 'react';
import type { RootState } from '../../store';
import { useSelector, useDispatch } from 'react-redux';
import { getUserDataSuccess } from '../../store/user';
import './app.css';

const App1 = React.lazy(() => import("app/App"));

export const App = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.user);
  const handleLogin = () => {
    dispatch(getUserDataSuccess({ name: 'John Doe' }));
  };

  return (
    <div>

        {!user.isLoggedIn && <div>
            <button onClick={handleLogin}>Login</button>
          </div>
        }
        {user.isLoggedIn && 
        <Suspense fallback="Loading...">
          <App1 />
        </Suspense>
      }

    </div>
)
  };