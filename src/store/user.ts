import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

export interface UserState {
  isLoading: boolean;
  isLoggedIn: boolean;
  userData: {};
}

const initialState: UserState = {
  isLoading: false,
  isLoggedIn: false,
  userData: {},
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    getUserData: (state) => {
      state.isLoading = true;
    },
    getUserDataSuccess: (state, action: PayloadAction<{ name: string }>) => {
      state.userData = action.payload;
      state.isLoggedIn = true;
      state.isLoading = false;
    },
  },
});

// Action creators are generated for each case reducer function
export const { getUserData, getUserDataSuccess } = userSlice.actions;

export default userSlice.reducer;
